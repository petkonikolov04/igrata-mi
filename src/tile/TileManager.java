package tile;

import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import javax.imageio.ImageIO;

import main.GamePanel;

public class TileManager {

	GamePanel gp;
	public Tile[] tile;
	public int mapTileNum[][];
	public int gemX;
	public int gemY;
	public String[] maps = {"map01.txt", "map02.txt", "map03.txt", "map04.txt", "map05.txt", "map06.txt", "map07.txt", "map08.txt", "map09.txt", "map10.txt"};
	public String currentMap;
	
	public TileManager(GamePanel gp) {
		
		this.gp = gp;
		
		tile = new Tile[15];
		mapTileNum = new int [gp.maxScreenCol][gp.maxScreenRow];
		
		getTileImage();
		loadMap();
	}
	
	public String getMap(){
	    Random rand = new Random();
	    int index = rand.nextInt(maps.length);
	    return maps[index];
	}
	
	public void getTileImage() {
		
		try {
			
			tile[0] = new Tile();
			tile[0].image = ImageIO.read(getClass().getResourceAsStream("/tiles/wall.png"));
			
			tile[3] = new Tile();
			tile[3].image = ImageIO.read(getClass().getResourceAsStream("/tiles/gem_2.png"));
				
			tile[1] = new Tile();
			tile[1].image = ImageIO.read(getClass().getResourceAsStream("/tiles/rock1.png"));
			tile[1].collision = true;
			
			
			
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void loadMap() {
		
		currentMap = getMap();
		
		try {
			InputStream is = getClass().getResourceAsStream("/maps/" + currentMap);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			
			int col = 0;
			int row = 0;
			
			while(col < gp.maxScreenCol && row < gp.maxScreenRow) {
				
				String line = br.readLine(); //chete red text 
				
				while(col < gp.maxScreenCol) {
					
					String numbers[] = line.split(" "); // vzima chislata ot kartata edno po edno
					
					int num = Integer.parseInt(numbers[col]);
					
					mapTileNum[col][row] = num;
					col++;
				}
				if(col == gp.maxScreenCol) {
					col = 0;
					row++;
				} // skanira textoviq fail dokato loopa ne svurshi
			}
			br.close();
			
		}catch(Exception e) {
			
		}
	}
	public void draw(Graphics2D g2) {
		
		int col = 0;
		int row = 0;
		int x = 0;
		int y = 0;
		
		while (col < gp.maxScreenCol && row < gp.maxScreenRow) {
			
			int tileNum = mapTileNum[col][row]; // map datata se suhranqva vuv mapTileNum[][]
			
			g2.drawImage(tile[tileNum].image, x, y, gp.tileSize, gp.tileSize, null);
			col++;
			x += gp.tileSize;
			
			if(tileNum == 3) {
				gemX = x;
				gemY = y;
			}
			
			if(col == gp.maxScreenCol) {
				col = 0;
				x = 0;
				row ++;
				y += gp.tileSize;
			}
		}
	}
}













