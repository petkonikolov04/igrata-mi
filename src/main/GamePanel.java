package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import entity.Player;
import tile.TileManager;

public class GamePanel extends JPanel implements Runnable{
	
	//nastroiki
	final int originalTileSize = 16; // 16x16
	final int scale = 3;
	
	public final int tileSize = originalTileSize * scale; // 48x48
	public final int maxScreenCol = 16;
	public final int maxScreenRow = 12;	
	public final int screenWidth = tileSize * maxScreenCol;
	public final int screenHeight = tileSize * maxScreenRow;
	
	//FPS
	int FPS = 60;
	
	TileManager tileM = new TileManager(this);
	KeyHandler keyH = new KeyHandler();
	Thread gameThread; 
	public CollisionChecker cChecker = new CollisionChecker(this);
	Player player = new Player(this, keyH);
	
	public GamePanel() {
		this.setPreferredSize(new Dimension(screenWidth, screenHeight)); //setva razmera na Panela
		this.setBackground(Color.black);
		this.setDoubleBuffered(true); //improved renderings
		this.addKeyListener(keyH);
		this.setFocusable(true); //s tova GamePanel poluchava key imput-ite
	}
	
	public void startGameThread() {
		
		gameThread = new Thread(this);
		gameThread.start();
	}

	@Override
//	public void run() {
//		
//		double drawInterval = 1000000000/FPS; // 1 sekunda / 60(fps)
//		double nextDrawTime = System.nanoTime() + drawInterval;
//		
//		while(gameThread != null) {
//			
//			// Obnovqva kakvo se sluchva na ekrana
//			update();
//			
//			// Pokazva promeneniq ekran
//			repaint();
//			
//			
//			
//			try {
//				double remainingTime = nextDrawTime = System.nanoTime();
//				remainingTime = remainingTime/1000000; // sleep priema chisloto kato milisekundi
//				
//				if(remainingTime < 0) {
//					remainingTime = 0;
//				} // nqma kak vremeto za pauza da e pod 0
//				
//				Thread.sleep((long) remainingTime); //pauzira game loop-a
//				
//				nextDrawTime += drawInterval;
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}	
//	}                       S TOZI KOD NE RABOTESHE PROGRAMATA I GEROICHETO NE SE DVIJESHE
	public void run() {
		
		double drawInterval = 1000000000/FPS;
		double delta = 0;
		long lastTime = System.nanoTime();
		long currentTime;
		
		while(gameThread != null) {
			
			currentTime = System.nanoTime();
			
			delta += (currentTime - lastTime) / drawInterval; 
			
			lastTime = currentTime;
			
			if(delta >= 1) {
				update(tileM);
				repaint();
				delta--;
				//vseki loop dobavq minaloto vreme / drawInterval
				//kogato delta stinge 1(drawIntervala) se repaintva i resetva delta 
			}
			
			
		}
	}
	public void update(TileManager tiles) {
		
		
		player.update(tiles);
		
	}
	public void paintComponent(Graphics g) {
		
		super.paintComponent (g);
		
		Graphics2D g2 = (Graphics2D)g;
		
		tileM.draw(g2);
		
		player.draw(g2);
		
		g2.dispose();
	}
}















